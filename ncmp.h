#pragma once

#include <string.h>
#include <stdlib.h>
#include <stddef.h>

#define $ncmp_dec(type) \
	int ncmp_##type##s (const void *a, const void *b);

#define $ncmp_dec_by_member(type, member_name, member_type) \
	int ncmp_##type##s_by_##member_name (const void *a, const void *b);

#define $ncmp_dec_by_member_using_func(type, member_name, member_type, func) \
	int ncmp_##type##s_by_##member_name##_using_##func (const void *a, const void *b);

#define $ncmp_def(type) \
	int ncmp_##type##s (const void *a, const void *b) { \
		return (*(const type *) a - *(const type *) b); \
	}

#define $ncmp_def_reverse(type) \
	int ncmp_##type##s_reverse (const void *a, const void *b) { \
		return (*(const type *) b - *(const type *) a); \
	}

#define $ncmp_def_by_member(type, member_name, member_type) \
	int ncmp_ ## type ## s_by_ ## member_name (const void *a, const void *b) { \
		size_t member_offset = offsetof (type, member_name); \
		auto a_member = (member_type *) ((char *) a + member_offset); \
		auto b_member = (member_type *) ((char *) b + member_offset); \
		return *a_member - *b_member; \
	}

#define $ncmp_def_by_member_reverse(type, member_name, member_type) \
	int ncmp_##type##s_by_##member_name (const void *a, const void *b) { \
		size_t member_offset = offsetof (type, member_name); \
		member_type *a_member = (member_type *) ((char *) a + member_offset); \
		member_type *b_member = (member_type *) ((char *) b + member_offset); \
		return *b_member - *a_member; \
	}

#define $ncmp_def_by_member_using_func(type, member_name, member_type, func) \
	int ncmp_##type##s_by_##member_name##_using_##func (const void *a, const void *b) { \
		size_t member_offset = offsetof (type, member_name); \
		member_type *a_member = (member_type *) ((char *) a + member_offset); \
		member_type *b_member = (member_type *) ((char *) b + member_offset); \
		return func (a_member, b_member); \
	}

#define $ncmp_def_by_member_reverse_using_func(type, member_name, member_type, func) \
	int ncmp_##type##s_by_##member_name##_using_##func (const void *a, const void *b) { \
		size_t member_offset = offsetof (type, member_name); \
		member_type *a_member = (member_type *) ((char *) a + member_offset); \
		member_type *b_member = (member_type *) ((char *) b + member_offset); \
		return func (b_member, a_member); \
	}

#define $ncmp_find_by_member(arr, n_arr, val, compar, _member) \
	({ \
	 	typeof (*arr) *res = 0; \
		size_t offset = offsetof (typeof (*(arr)), _member); \
		for (typeof (*arr) *elem = (arr); \
				elem < (arr) + (n_arr); elem++) { \
			char *base = (char *) elem; \
			typeof ((*arr)._member) *p = (typeof ((*arr)._member) *) base + offset; \
			if (0 == ((compar) (val, p))) { \
				res = elem; \
				break; \
			} \
		} \
		res; \
	})

// FIXME: make this and $find_by_member call on the same function.
// Reduce repetition.
#define $ncmp_find_by_member_r(arr, n_arr, val, compar, _member, arg) \
	({ \
	 	typeof (*arr) *res = 0; \
		size_t offset = offsetof (typeof (*(arr)), _member); \
		for (typeof (*arr) *elem = (arr); \
				elem < (arr) + (n_arr); elem++) { \
			char *base = (char *) elem; \
			typeof ((*arr)._member) *p = (typeof ((*arr)._member) *) base + offset; \
			if (0 == ((compar) (val, *p, arg))) { \
				res = elem; \
				break; \
			} \
		} \
		res; \
	})

$ncmp_dec (int);
$ncmp_dec (char);
$ncmp_dec (size_t);
$ncmp_dec (ssize_t);
int ncmp_strs (const void *a, const void *b);
int ncmp_strs_reverse (const void *a, const void *b);
int ncmp_strs_with_nulls (const void *a, const void *b);
int ncmp_strs_with_nulls_reverse (const void *a, const void *b);
int ncmp_strs_by_len (const void *a, const void *b);
int ncmp_strs_by_len_reverse (const void *a, const void *b);
int ncmp_strs_with_nulls_by_len (const void *a, const void *b);
int ncmp_strs_with_nulls_by_len_reverse (const void *a, const void *b);
