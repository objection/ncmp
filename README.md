# ncmp

This is just a few comparison functions, and some macros to make
simple ones. It might not work on all compilers, because I've used gcc
stuff.

# Macros

To make a simple integer comparison function, call `$dec_cmp` in a
header file and `$def_cmp` in its .c file. There's probably a way to
do it in a oner, but I'm not interested right now.

I've declared and defined a few you'll probably want, like int, size_t,
etc. You can see in the header file.

One thing to note is your `type` has to be a single token, ie, it
can't be eg `unsigned int`. That's just the way the preprocessor
works. Having said that, I've seen macro wizardry before. Probably
knows how to paste together a variable number of tokens. I don't,
though.

Solution: use single tokens, like the ones in stdint.h, eg, uint8_t,
or make your own typedefs (I think u8 is pretty sexy).

## $dec_cmp / $def_cmp

```c
#define $dec_cmp(type)
#define $def_cmp(type)
```

These two macros declare and define a simple integer comparison
function respectively. `type` is the type of the thing being compared
and also part of its name.

Put the first one in the header file and the second in the
corresponding .c file.

## $dec_cmp_reverse / $def_cmp_reverse

```c
#define $dec_cmp_reverse(type)
#define $def_cmp_reverse(type)
```

Same as before, except the functions these generate will produce a
reverse sort.

## $def_cmp_by_member / $def_cmp_by_member_reverse

```c
#define $def_cmp_by_member(type, member_name, member_type) \
```

These make simple comparison functions that compare based on a member
of a struct. Say you have a:

```c
typedef struct thing {
	int a;
	int b;
} thing;
```

You can write a comparison function that compares by the value of b
like this:

```c
$dec_cmp (thing);
$dec_cmp_by_member (my_struct, my_member_name, my_member_type);
```

You'll end up with a comparison function called `ncmp_thing_by_b`.
You can use it to sort the struct by the value of thing.b.

## $ncmp_find_by_member

I'll just leave this here, even though using dec_cmp_by_member is more
generic.

This finds an element in an array of structs by one of its member. It
seems to work.

## $ncmp_find_by_member_r

Ditto, except it also takes a struct, like qsort_r. The r is for
"reentrant".

# Functions

I won't bother revealing the signatures. Comparison functions are all
the same:

```c
int ncmp_whatever (const void *a, const void *b);
```

## ncmp_strs

String sort.

## ncmp_strs_reverse

Reverse string sort.

## ncmp_strs_with_nulls

String sort that also sorts NULL strings. They end up at the
beginning.

## ncmp_strs_with_nulls_reverse

Reverse string sort that also sorts NULL strings. They end up at the
end.

## ncmp_strs_by_len

Compares by string length, ie it runs strlen on each string. It
doesn't consider NULLs.

## ncmp_strs_by_len_reverse

Same as before, but in reverse.

## ncmp_strs_by_len_with_nulls

Same as ncmp_strs_by_len, except deals with NULLS.

## ncmp_strs_by_len_with_nulls_reverse

Same as before, but in reverse.

