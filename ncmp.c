#if 0
	gcc -DTEST -ggdb3 -Wextra -Wno-missing-field-initializers \
		-o ncmp \
		ncmp.c
	exit
#endif
#define _GNU_SOURCE
#include "ncmp.h"

$ncmp_def (int);
$ncmp_def_reverse (int);
$ncmp_def (char);
$ncmp_def_reverse (char);
$ncmp_def (size_t);
$ncmp_def_reverse (size_t);
$ncmp_def (ssize_t);
$ncmp_def_reverse (ssize_t);

int ncmp_strs (const void *a, const void *b) {
	return strcmp (*(const char **) a, *(const char **) b);
}

int ncmp_strs_reverse (const void *a, const void *b) {
	return strcmp (*(const char **) b, *(const char **) a);
}

// Sometimes you'll have an array of strings where some are 0.
int ncmp_strs_with_nulls (const void *a, const void *b) {
	const char **aa = (const char **) a;
	const char **bb = (const char **) b;
	if (!*aa && !*bb) return 0;
	if (!*aa && *bb) return -1;
	if (*aa && !*bb) return 1;
	return strcmp (*aa, *bb);
}

// Sometimes you'll have an array of strings where some are 0.
int ncmp_strs_with_nulls_reverse (const void *a, const void *b) {
	const char **aa = (const char **) a;
	const char **bb = (const char **) b;
	if (!*aa && !*bb) return 0;
	if (!*aa && *bb) return 1;
	if (*aa && !*bb) return -1;
	return strcmp (*bb, *bb);
}

int ncmp_strs_by_len (const void *a, const void *b) {
	return strlen (*(const char **) a) - strlen (*(const char **) b);
}

int ncmp_strs_by_len_reverse (const void *a, const void *b) {
	return strlen (*(const char **) b) - strlen (*(const char **) a);
}

int ncmp_strs_with_nulls_by_len (const void *a, const void *b) {
	const char **aa = (const char **) a;
	const char **bb = (const char **) b;
	int n_a = *aa? strlen (*aa): 0;
	int n_b = *bb? strlen (*bb): 0;
	return n_a - n_b;
}

int ncmp_strs_with_nulls_by_len_reverse (const void *a, const void *b) {
	const char **aa = (const char **) a;
	const char **bb = (const char **) b;
	int n_a = *aa? strlen (*aa): 0;
	int n_b = *bb? strlen (*bb): 0;
	return n_b - n_a;
}

#if defined TEST

// These aren't really "tests".

#include <stdio.h>
int main (int argc, char **argv) {

#define $n(x) ((sizeof(x)/sizeof((x)[0])) / ((size_t)(!(sizeof(x) % sizeof((x)[0])))))
#define $fori(_idx, _max) \
	for (typeof (_max) _idx = 0; _idx < _max; _idx++)
#define $print_a(a, n, fmt_specifier, print_first) \
	({ \
		printf ("\n" print_first "\n"); \
		$fori (i, n) { \
			if (!a[i]) \
				printf ("\t0\n"); \
			else \
				printf ("\t" fmt_specifier "\n", a[i]); \
		} \
	})

#define $check(arr, func, fmt_specifier, msg) \
	qsort (arr, $n (arr), sizeof *(arr), func); \
	$print_a (arr, $n (arr), fmt_specifier, msg);

	$ncmp_def (int);

	int these[] = {4, 5, 243, 23, 23, 4098, 32, 3, };
	char *these_strs[] = { "this", "that", "the other", };
	char *these_strs_with_nulls[] = { "this", "that", 0, "the other", 0, };

	// INTS
	$check (these, ncmp_ints, "%d", "ints forward");
	$check (these, ncmp_ints_reverse, "%d", "ints reverse");

	// STRS WITHOUT NULLS
	$check (these_strs, ncmp_strs, "%s",  "strs forward");
	$check (these_strs, ncmp_strs_reverse, "%s", "strs reverse");

	$check (these_strs, ncmp_strs_by_len, "%s",  "strs by len forward");
	$check (these_strs, ncmp_strs_by_len_reverse, "%s", "strs by len reverse");

	// STRS WITH NULLS
	$check (these_strs_with_nulls, ncmp_strs_with_nulls, "%s", "strs with NULLS");
	$check (these_strs_with_nulls, ncmp_strs_with_nulls_reverse, "%s",
			"strs with NULLS reverse");

	$check (these_strs_with_nulls, ncmp_strs_with_nulls_by_len, "%s",
			"strs with nulls by len forward");
	$check (these_strs_with_nulls, ncmp_strs_with_nulls_by_len_reverse, "%s",
			"strs with nulls by len reverse");

	typedef struct this {
		int hello;
		int that;
	} this;

	struct this these_thises[] = {
		{
			.hello = 10,
			.that = 15,
		},
		{
			.hello = 10,
			.that = 23,
		},
		{
			.hello = 10,
			.that = 5,
		},
	};
	$ncmp_dec (this);
	$ncmp_def_by_member (this, that, int);
	qsort (these_thises, $n (these_thises), sizeof *these_thises, ncmp_thiss_by_that);
	printf ("\nthises by that\n");
	$fori (i, $n (these_thises))
		printf ("\t{%d, %d}\n", these_thises[i].hello, these_thises[i].that);

	exit (0);
}
#endif


